from pydantic import BaseModel


class UserDBErrorSchema(BaseModel):
    success: bool = False
    msg: str = 'Problems with DB...'


class UserCreateErrorSchema(BaseModel):
    success: bool = False
    msg: str = 'This email was registered before'
