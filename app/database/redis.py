import redis.asyncio as redis
from app.configs.config import RedisSettings

redis_settings = RedisSettings()
r = redis.Redis(host=redis_settings.RD_SERVER, port=redis_settings.RD_PORT, password=redis_settings.RD_PASSWORD)
