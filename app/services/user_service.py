from sqlalchemy import exc
from app.database.models.user import User
from app.exceptions.custom import MyHTTPException
from app.services.sql_service import SQLService
from app.utils import sqlalchemy_object_to_dict


class UserService(SQLService):
    async def create_user(self, user: User):
        r = await self.check_user_by_email(user)
        if not r:
            try:
                self.db.add(user)
                self.db.commit()
                return True
            except exc.SQLAlchemyError as e:
                print(e)
                raise MyHTTPException(code=500, success=False, msg="Problems with DB...")
        else:
            return False

    async def check_user_by_email(self, user: User):
        try:
            q = self.db.query(User).filter(User.email == user.email).first()
            if q:
                return True
            else:
                return False
        except exc.SQLAlchemyError as e:
            print(e)
            raise MyHTTPException(code=500, success=False, msg="Problems with DB...")

    async def get_user_by_email(self, user: User):
        try:
            q = self.db.query(User).filter(User.email == user.email).first()
            if q:
                user = await sqlalchemy_object_to_dict(q)
                return user
            else:
                return {}
        except exc.SQLAlchemyError as e:
            print(e)
            raise MyHTTPException(code=500, success=False, msg="Problems with DB...")

    async def get_all_users(self):
        try:
            # Отправить запрос
            q = self.db.query(User).all()
            list_of_users = []
            # Преобразовать его в список
            for i in q:
                user = await sqlalchemy_object_to_dict(i)
                list_of_users.append(user)
            return list_of_users
        except exc.SQLAlchemyError as e:
            print(e)
            raise MyHTTPException(code=500, success=False, msg="Problems with DB...")
