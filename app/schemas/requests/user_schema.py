from pydantic import BaseModel, EmailStr


class UserCreateSchema(BaseModel):
    name: str = 'Abay'
    surname: str = 'Sadykov'
    email: EmailStr = 'pythondelay@gmail.com'
