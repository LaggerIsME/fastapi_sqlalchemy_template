# Преобразовать SQLAlchemy-объект в Python dict
async def sqlalchemy_object_to_dict(sqlalchemy_object):
    sqlalchemy_object_dict = sqlalchemy_object.__dict__
    del sqlalchemy_object_dict['_sa_instance_state']
    return sqlalchemy_object_dict
