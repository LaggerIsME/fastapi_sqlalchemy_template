from fastapi import Depends
from sqlalchemy.orm import Session
from app.database.db import get_db


# SQL-сервисы
class SQLService:
    def __init__(self, db: Session = Depends(get_db)):
        self.db = db

