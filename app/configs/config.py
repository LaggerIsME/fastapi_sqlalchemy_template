from pydantic_settings import BaseSettings
import os
from dotenv import load_dotenv
# Загрузка файла среды
load_dotenv(".env")


# Настройки PostgreSQL
class PostgresSettings(BaseSettings):
    PG_USER: str = os.getenv("PG_USER")
    PG_PASSWORD: str = os.getenv("PG_PASSWORD")
    PG_SERVER: str = os.getenv("PG_SERVER")
    PG_DB: str = os.getenv("PG_DB")


# Настройки Redis
class RedisSettings(BaseSettings):
    RD_SERVER: str = os.getenv("RD_SERVER")
    RD_PASSWORD: str = os.getenv("RD_PASSWORD")
    RD_PORT: int = os.getenv("RD_PORT")
