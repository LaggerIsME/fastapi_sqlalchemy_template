![image](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue)
![image](https://img.shields.io/badge/fastapi-109989?style=for-the-badge&logo=FASTAPI&logoColor=white)
![image](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white)
![image](https://img.shields.io/badge/redis-CC0000.svg?&style=for-the-badge&logo=redis&logoColor=white)
![image](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)
# FastAPI SQLAlchemy Default Template
Данный репозиторий это простой фундамент для API-сервисов на базе фреймворка FastAPI, чтоб не тратить постоянно много времени на подключение базовых вещей, как SQLAlchemy, Pydantic, Redispy, Alembic, Routers. В будущем будет подключена админ-панель через SQLAdmin.
## Структура проекта
```
├── app # Основная директория проекта
│   ├── configs # Модуль, содержащий конфиги с переменными для баз данных и т.д. 
│   │   ├── config.py # Конфиги PostgreSQL, Redis
│   │   ├── __init__.py
│   ├── database # Модуль, содержащий модели для баз данных, а также их настройки
│   │   ├── db.py # Настройки SQLAlchemy для работы с PostgreSQL
│   │   ├── __init__.py
│   │   ├── models # Модуль, содержащий SQLAlchemy-модели
│   │   │   ├── __init__.py
│   │   │   └── user.py # Базовая модель User-а
│   │   └── redis.py # Настройки Redis-py для работы с Redis
│   ├── exceptions # Модуль, содержащий особые исключения
│   │   ├── custom.py # Особые HTTPException-ы
│   │   └── __init__.py
│   ├── routers # Модуль, содержащий контроллеры и их версии
│   │   ├── __init__.py
│   │   └── v1
│   │       ├── __init__.py
│   │       └── userRouter.py # Роутер для endpoint-ов User-а
│   ├── schemas # Модуль, содержащий схемы для OpenAPI документации и Pydantic-валидации
│   │   ├── errors # Модуль, содержащий схемы для ошибок
│   │   │   ├── __init__.py
│   │   │   └── userSchema.py
│   │   ├── __init__.py
│   │   ├── requests # Модуль, содержащий схемы для запросов
│   │   │   ├── __init__.py
│   │   │   └── userSchema.py
│   │   └── responses # Модуль, содержащий схемы для ответов
│   │       ├── __init__.py
│   │       └── userSchema.py
│   ├── services # Модуль, содержащий сервисы для отделения бизнес-логики от роутеров
│   │   ├── __init__.py
│   │   ├── sqlService.py # Родительский сервис для сервисов, которые будут работать с SQLAlchemy
│   │   └── userService.py # Сервис для работы с User-ом
│   ├── __init__.py
│   ├── main.py # Основной файл для запуска проекта
│   └── utils.py # Утилиты
├── .dockerignore
├── .env 
├── .gitignore
├── docker-compose.yml
├── Dockerfile
├── README.md
├── requirements.txt
```
## Инструменты и библиотеки
* FastAPI
* SQLAlchemy
* Pydantic
* PostgreSQL
* Redis
* Docker
* Docker Compose
## Зависимости
* Python 3.10+
* SQLAlchemy 2.0+
* Redis 7.0+
* PostgreSQL 15+
## Установка
* Клонировать репозиторий: `git clone https://gitlab.com/LaggerIsME/fastapi_sqlalchemy_template`
* Скачать и установить Docker и Docker-Compose: https://docs.docker.com/engine/install/
* Перейти в директорию проекта: `cd ~/fastapi_sqlalchemy_template`
* Запустить проект с помощью команды:
* * Linux: `docker compose up -d --build`
* * MacOS, Windows: `docker-compose up -d --build`

Данный проект будет доступен на http://localhost:8000/api/
