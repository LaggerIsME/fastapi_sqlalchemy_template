from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from app.database.db import get_db
from app.database.models.user import User
from app.exceptions.custom import MyHTTPException
from app.schemas.errors.user_schema import UserCreateErrorSchema, UserDBErrorSchema
from app.schemas.requests.user_schema import UserCreateSchema
from app.schemas.responses.user_schema import UserCreateResponseSchema, UserGetAllResponseSchema
from app.services.user_service import UserService

user_router = APIRouter(tags=["Users"], prefix='/users')


@user_router.get("/get_all_users", response_model=UserGetAllResponseSchema, responses={
    500: {"model": UserDBErrorSchema, "description": "Problems with DB"}
})
async def get_all_users(db: Session = Depends(get_db)):
    r = await UserService(db).get_all_users()
    # Если прилетел Exception
    if isinstance(r, MyHTTPException):
        return r
    else:
        return JSONResponse(status_code=200, content={"success": True, "msg": r})


@user_router.post("/create_user", response_model=UserCreateResponseSchema, responses={
    409: {"model": UserCreateErrorSchema, "description": "Conflict in DB"},
    500: {"model": UserDBErrorSchema, "description": "Problems with DB"}
})
async def create_user(userSchema: UserCreateSchema, db: Session = Depends(get_db)):
    user = User(name=userSchema.name, surname=userSchema.surname, email=userSchema.email)
    r = await UserService(db).create_user(user)
    # Если прилетел Exception
    if isinstance(r, MyHTTPException):
        raise r
    elif not r:
        raise MyHTTPException(code=409, success=False, msg="This email was registered before")
    else:
        return JSONResponse(status_code=200, content={"success": True, "msg": "User was registered"})
