from fastapi import FastAPI, APIRouter, Request
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware

from app.database.db import init_db
from app.routers.v1.user_router import user_router
from app.exceptions.custom import MyHTTPException


# Создание приложения на пути /api/
app = FastAPI(docs_url="/api/docs", redoc_url="/api/redoc", openapi_url="/api/openapi.json")

# Подключение CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

# Общий роутер, на который цепляем все остальные endpoint-ы
api_router = APIRouter(prefix="/api")


# Корневой endpoint
@api_router.get("/")
async def root():
    return {"message": "FastAPI template of LaggerIsME(https://github.com/LaggerIsME)"}


# Действия при старте приложения
@app.on_event("startup")
async def startup():
    init_db()


# Кастомные исключения
@app.exception_handler(MyHTTPException)
async def unicorn_exception_handler(request: Request, exc: MyHTTPException):
    return JSONResponse(status_code=exc.code, content={"success": exc.success, "msg": exc.msg})


# Добавляем роутеры на общий /api
api_router.include_router(user_router)
# Добавляем общий роутер к приложению
app.include_router(api_router)
