class MyHTTPException(Exception):
    def __init__(self, code: int, success: bool, msg: str):
        self.code = code
        self.success = success
        self.msg = msg

