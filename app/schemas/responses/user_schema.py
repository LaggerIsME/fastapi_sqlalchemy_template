from typing import List
from pydantic import BaseModel


class UserCreateResponseSchema(BaseModel):
    success: bool = True
    msg: str = 'User was registered'


class UserGetAllResponseSchema(BaseModel):
    success: bool
    msg: List[dict]


